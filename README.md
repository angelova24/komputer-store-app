# Komputer Store App
```gr
 First Assignment for Noroff JS-Fundamentals module. 
 Simple web page where you can work to gain money which you send to a bank balance so you can buy a new laptop.
 ```

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Install
```sh
Live Server
```
## Usage
```sh
Live Server
```

## Maintainers

[@angelova24](https://gitlab.com/angelova24)