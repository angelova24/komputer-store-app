//get elements from html file
const balanceElement = document.getElementById("balance");
const getLoanButtonElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const bankButtonElement = document.getElementById("bank");
const workButtonElement = document.getElementById("work");
const laptopSelectionElement = document.getElementById("laptop");
const laptopFeauturesElement = document.getElementById("features");
const laptopStockElement = document.getElementById("stock");
const laptopNameElement = document.getElementById("laptopName");
const laptopImageElement = document.getElementById("image");
const laptopDescriptionElement = document.getElementById("description");
const laptopPriceElement = document.getElementById("price");
const buyButtonElement = document.getElementById("buy");
const secretLoanTextElement = document.getElementById("secretLoanText");
const secretLoanAmountElement = document.getElementById("secretLoanAmount");
const secretButtonElement = document.getElementById("secretButton");

let laptops = []; //collection for laptops from API
let payBalance = 0; //pay balance in section work
let bankBalance = 0; //bank balance in section bank
let outstandingLoan = 0; // amount of outstanding loan in section bank
let gotLoan = false;

//create secret button 'repay loan' for section work
const mySecretButtonElement = document.createElement("button");
mySecretButtonElement.id = "secret";
mySecretButtonElement.innerText = "Repay Loan";

//function to format number as currency
function setCurrency(number) {
  let amount = new Intl.NumberFormat("de-DE", {
    style: "currency",
    currency: "EUR",
  }).format(number);
  return amount;
}

//function to clear all loan info(secret elements)
function coverOutstandingLoan() {
  outstandingLoan = 0;
  gotLoan = false;
  secretLoanTextElement.innerText = "";
  secretLoanAmountElement.innerText = "";
  secretButtonElement.removeChild(mySecretButtonElement);
}

//retrieving data from an API and store it in laptops collection
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToSection(laptops));

const addLaptopsToSection = (laptops) => {
  laptops.forEach((x) => addLaptopToSection(x));
  laptopFeauturesElement.innerText = laptops[0].specs.join("\n");
  laptopStockElement.innerText = laptops[0].stock;
  laptopImageElement.src = `pictures/${laptops[0].id}.jpg`;
  laptopNameElement.innerText = laptops[0].title;
  laptopDescriptionElement.innerText = laptops[0].description;
  laptopPriceElement.innerText = setCurrency(laptops[0].price);
};

//fill out options in section Laptops
const addLaptopToSection = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopSelectionElement.appendChild(laptopElement);
};

//change features list in section laptops and fill out section info
const handleLaptopChange = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];
  laptopFeauturesElement.innerText = selectedLaptop.specs.join("\n");
  laptopStockElement.innerText = selectedLaptop.stock;
  laptopImageElement.src = `pictures/${selectedLaptop.id}.jpg`;
  laptopNameElement.innerText = selectedLaptop.title;
  laptopDescriptionElement.innerText = selectedLaptop.description;
  laptopPriceElement.innerText = setCurrency(selectedLaptop.price);
};

//add 100 to pay balance after every click on work button
const handleWorkButton = () => {
  payBalance += 100;
  payElement.innerText = setCurrency(payBalance);
};

//increase bank balance with pay balance after click on bank button
//in case there is outstanding loan: 10% of the pay balance MUST first cover the outstanding loan
const handleBankButton = () => {
  //if there is no outstanding loan increase bank balance with the amount in pay balance
  if (gotLoan == false) {
    bankBalance += payBalance;
  } else {
    //there is outstanding loan and 10% must cover the outstanding loan
    bankBalance += payBalance * 0.9; //increase bank balance with 90% of pay balance
    outstandingLoan -= payBalance * 0.1; //decrease outstanding loan with 10% of pay balance
    secretLoanAmountElement.innerText = setCurrency(outstandingLoan); //update outstanding loan info
    if (outstandingLoan <= 0) {
      //outstanding loan is fully covered clear loan info
      //if 10% are more than the oustanding loan increase bank balance with the rest
      if (outstandingLoan < 0) {
        bankBalance -= outstandingLoan;
      }
      coverOutstandingLoan();
    }
  } //clear pay balance and update bank balance info
  payBalance = 0;
  payElement.innerText = setCurrency(payBalance);
  balanceElement.innerText = setCurrency(bankBalance);
};

//after click on 'get a loan' show popup box to enter an amount
//in case there is already outstanding loan show alert(cannot get two loans at once)
//check if meet the requirements(cannot get a loan more than double of bank balance)
//make the amount visible in section bank and show the secret 'pay loan' button in section work
const handleGetLoanButton = () => {
  if (gotLoan == false) {
    //enter an amount
    let amount = prompt(
      "Please enter the amount of money you wish to borrow: "
    );
    //if the input is not valid or it's a negative value 
    if (isNaN(amount) || amount <= 0) {
      alert("Please enter a valid number bigger than 0");
    } //if the amount is less or equal than double of bank balance get a loan
    else if (parseFloat(amount) <= bankBalance * 2) {
      gotLoan = true;
      outstandingLoan += parseFloat(amount);
      bankBalance += outstandingLoan;
      //show the secret loan info in section bank
      secretLoanTextElement.innerText = "Outstanding loan:";
      secretLoanAmountElement.innerText = setCurrency(outstandingLoan);
      //show the secret 'pay loan' button in section work and update bank balance info
      secretButtonElement.appendChild(mySecretButtonElement);
      balanceElement.innerText = setCurrency(bankBalance);
    } //if the amount is more than double of bank balance show alert
    else if (parseFloat(amount) > bankBalance * 2) {
      alert("You cannot get a loan more than double of your bank balance");
    }
  } else {
    //there is already outstanding loan show alert
    alert(
      "You cannot get more loan. You have to pay back the outstanding loan!"
    );
  }
};

//cover the outstanding loan with the pay balance
const handlePayLoanButton = () => {
  outstandingLoan -= payBalance; //decrease outstanding loan with the pay balance
  //clear pay balance and update outstanding loan info
  payBalance = 0;
  payElement.innerText = setCurrency(payBalance);
  secretLoanAmountElement.innerText = setCurrency(outstandingLoan);
  //if outstanding loan is fully covered clear loan info
  if (outstandingLoan <= 0) {
    //if there is rest increase bank balance with it
    if (outstandingLoan < 0) {
      bankBalance -= outstandingLoan;
      balanceElement.innerText = setCurrency(bankBalance);
    }
    coverOutstandingLoan();
  }
};

//buy a laptop if there is enough money
const handleBuyButton = () => {
  //find the selected laptop from section laptops
  let selectedLaptop = laptops.find(
    (laptop) => laptop.title == laptopNameElement.innerText
  );
  //if there is enough money in bank balance
  if (bankBalance >= selectedLaptop.price) {
    bankBalance -= parseFloat(selectedLaptop.price); //reduce the bank balance
    balanceElement.innerText = setCurrency(bankBalance);
    const indexOfSelectedLaptop = laptops.indexOf(selectedLaptop);
    laptops[indexOfSelectedLaptop].stock -= 1; //reduce the available pieces of the laptop
    laptopStockElement.innerText = selectedLaptop.stock; //show the new availability
    if (selectedLaptop.stock == 0) {
      //if there is no more available pieces remove the laptop from the selection and show the rest
      laptops = laptops.filter((laptop) => laptop.id != selectedLaptop.id);
      laptopSelectionElement.replaceChildren("");
      addLaptopsToSection(laptops);
    }
    alert("Congratulation, you have a new laptop!");
  } else {
    //there is no enough money
    alert("You cannot afford the laptop!");
  }
};

laptopSelectionElement.addEventListener("change", handleLaptopChange);
workButtonElement.addEventListener("click", handleWorkButton);
bankButtonElement.addEventListener("click", handleBankButton);
getLoanButtonElement.addEventListener("click", handleGetLoanButton);
mySecretButtonElement.addEventListener("click", handlePayLoanButton);
buyButtonElement.addEventListener("click", handleBuyButton);
